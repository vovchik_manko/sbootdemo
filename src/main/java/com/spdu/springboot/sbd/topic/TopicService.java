package com.spdu.springboot.sbd.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicService {

    @Autowired
    private TopicDao topicDao;

    public List<Topic> getTopics() {
        return topicDao.getTopics();
    }

    public Topic getTopic(int id) {
        return topicDao.getTopic(id);
    }

    public void addTopic(Topic topic) {
        topicDao.addTopic(topic);
    }

    public void updateTopic(int id, Topic topic) {
        topicDao.updateTopic(id, topic);
    }

    public void deleteTopic(int id) {
        topicDao.deleteTopic(id);
    }
}
